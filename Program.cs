﻿using System;
using System.Collections.Generic;

namespace AreSimilar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        bool areSimilar(int[] a, int[] b) {
            List<int> l = new List<int>();
            List<int> a1 = new List<int>(a);
            List<int> b1 = new List<int>(b);
            
            
            for(int i = 0; i < a.Length; i++){
                if(a[i] != b[i]){
                    l.Add(i);
                }
            }
            
            if(l.Count>2){
                return false;
            }
            if(l.Count == 2){
                if(b[l[0]] == a[l[1]] && b[l[1]] == a[l[0]]){
                    return true;
                }
                return false;
            }
            return true;
            
        }

    }
}
